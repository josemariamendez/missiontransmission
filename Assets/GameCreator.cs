﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameCreator : MonoBehaviour {

    public GameObject racePrefab;
    private GameObject raceInstance;

	// Use this for initialization
	void OnEnable () {
		raceInstance = GameObject.Instantiate(racePrefab);
	}
	
	// Update is called once per frame
	void OnDisable () {
        Destroy(raceInstance);
	}
}
