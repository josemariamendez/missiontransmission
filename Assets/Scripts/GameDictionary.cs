﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

static public class GameDictionary {

	// GAME STATES
#region GameStates

    public const int STATE_INIT = -1;
    public const int STATE_INTRO_IN = 0;
    public const int STATE_INTRO = 1;
	public const int STATE_INTRO_OUT = 2;
    public const int STATE_MENU_IN = 5;
    public const int STATE_MENU = 6;
    public const int STATE_MENU_OUT = 7;
    public const int STATE_PLAYER_CONFIG_IN = 10;
    public const int STATE_PLAYER_CONFIG = 11;
    public const int STATE_PLAYER_CONFIG_OUT = 12;
    public const int STATE_TUTORIAL_IN = 20;
    public const int STATE_TUTORIAL = 21;
    public const int STATE_TUTORIAL_OUT = 22;
    public const int STATE_GAME_IN = 30;
    public const int STATE_GAME = 31;
    public const int STATE_GAME_OUT = 32;
    public const int STATE_END_GAME_IN = 40;
    public const int STATE_END_GAME = 41;
    public const int STATE_END_GAME_OUT = 42;
    public const int STATE_PAUSE_IN = 50;
    public const int STATE_PAUSE = 51;
    public const int STATE_PAUSE_OUT = 52;
    public const int STATE_CREDITS_IN = 60;
    public const int STATE_CREDITS = 61;
    public const int STATE_CREDITS_OUT = 62;

#endregion

#region OPTIONS

	public const int OPTION_NONE = 0;
	public const int OPTION_YES = 1;
	public const int OPTION_NO = 2;
	public const int OPTION_CONTINUE = 3;
	public const int OPTION_CANCEL = 4;
	public const int OPTION_EXIT = 5;
	public const int OPTION_BACK = 6;
	public const int OPTION_START = 7;
	public const int OPTION_CREDITS = 8;
	public const int OPTION_PAUSE = 9;
	public const int OPTION_ENDGAME = 10;

#endregion

	public const float GAMEFSM_UPDATE_RATE = 0.03125f; // 32 times per second
	public const float RACEFSM_UPDATE_RATE = 0.0625f; // 16 times per second

	public const float FAIL_TIME_MAX = 0.5f;

	public static string webplayerQuitURL = "https://globalgamejam.org/";

}