﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InputController : MonoBehaviour {

    [System.Serializable]
    public class ButtonPressEvent : UnityEvent<string, int>{}

    [System.Serializable]
    public class DeltaChangeEvent : UnityEvent<float>{}

    public int playerIndex = 0;

    public uint maxIntervals = 10;
    public InputSequence[] sequences;
    public InputSequence sequence;

    public ButtonPressEvent OnInputFail;
    public ButtonPressEvent OnInputOk;
    public DeltaChangeEvent OnDeltaChange;

    private List<float> intervals = new List<float>();
    private uint buttonIndex = 0;
    private float currentInterval = 0;
    private float lastTimestamp = 0;
    private float delta = 0;
    private float lastDelta = 0;
    private string lastButtonDownName = string.Empty;
    public string previousButtonName = string.Empty;
    public string nextButtonName = string.Empty;
    public uint nextButtonIndex = 0;
    public uint previousButtonIndex = 0;


    /**
     * Difference between last interval and average interval.
     */
    public float Delta{
        get{return delta;}
    }   

    public float CurrentInterval{
        get{return currentInterval;}
    }  

    public void Awake(){
        if (sequence == null || sequence.buttons.Length == 0)
            Debug.LogError("Need a non-empty sequence to initialize InputController");
        else{
            buttonIndex = (uint)(sequence.buttons.Length-1);
            GetComponent<Animator>().runtimeAnimatorController = sequence.animationController;
            nextButtonName = sequence.buttons[0];
            nextButtonIndex = 0;
        }

    }

    public void SetRandomSequence(){
        int randomSequence = UnityEngine.Random.Range(0,sequences.Length);
        sequence = sequences[randomSequence];
        buttonIndex = (uint)(sequence.buttons.Length-1);
        GetComponent<Animator>().runtimeAnimatorController = sequence.animationController;
    }

    public void Update(){
        UpdateInput(Time.realtimeSinceStartup);
    }

    /**
     * Updates input. Pass a timestamp value.
     */
    void UpdateInput(float timestamp){

        // update current interval:
        currentInterval = timestamp - lastTimestamp;

        if (sequence != null){

            bool anyPressed = false;
            if (Input.GetButtonDown("Up"+playerIndex)){
                lastButtonDownName = "Up";
                anyPressed = true;
            }else if (Input.GetButtonDown("Down"+playerIndex)){
                lastButtonDownName = "Down";
                anyPressed = true;
            }else if (Input.GetButtonDown("Left"+playerIndex)){
                lastButtonDownName = "Left";
                anyPressed = true;
            }else if (Input.GetButtonDown("Right"+playerIndex)){
                lastButtonDownName = "Right";
                anyPressed = true;
            }
        
			// calculate next button index:
            nextButtonIndex = (buttonIndex+1) % (uint)sequence.buttons.Length;
            nextButtonName = sequence.buttons[nextButtonIndex];

            // if the user pressed any button:
            if (anyPressed){


                // if the pressed button is the next one:
                if (lastButtonDownName == sequence.buttons[nextButtonIndex]){
                    
                    AddInterval(currentInterval);

                    // update last press timestamp
                    lastTimestamp = timestamp;

                    buttonIndex = nextButtonIndex;
					OnInputOk.Invoke(lastButtonDownName, playerIndex);
                }
                // if the user failed:
                else{
					OnInputFail.Invoke(lastButtonDownName, playerIndex);
                }
            }

        }
        
        UpdateDelta();

    }

    /**
     * Updates the delta value: difference between the average interval and last interval. Positive if getting faster,
     * negative if getting slower.
     */
    void UpdateDelta(){

        if (intervals.Count > 0){

            delta = AverageIntervals() - intervals[intervals.Count-1];

            OnDeltaChange.Invoke(delta);
        }
    }

    /**
     * Adds a new button press interval to the intervals list. The list has a maximum size,
     * so once its size reaches maxIntervals all existing values will be shifted down and the new value
     * will be inserted as the last one in the array.
     */
    void AddInterval (float newInterval) {

        if (intervals.Count == maxIntervals){

            for (int i = 0; i < intervals.Count-1; ++i)
                intervals[i] = intervals[i+1];

            intervals[intervals.Count-1] = newInterval;

        }else{
            intervals.Add(newInterval);
        }
	}


    /**
     * Calculates the average of all intervals.
     */
    float AverageIntervals(){

        float avg = 0;
        for (int i = 0; i < intervals.Count; ++i)
            avg += intervals[i];

        avg /= intervals.Count;

        return avg;    
    }
}
