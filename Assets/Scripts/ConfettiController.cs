﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfettiController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GetComponent<ParticleSystem> ().Stop ();
		gameObject.SetActive (false);
	}

	public void StartConfettiRain(){
		gameObject.SetActive (true);
		GetComponent<ParticleSystem> ().Play ();
	}
	public void SuddenStopConfettiRain(){
		GetComponent<ParticleSystem> ().Stop ();
		gameObject.SetActive (false);
	}
	public void SoftStopConfettiRain(){
		GetComponent<ParticleSystem> ().Stop ();
	}
	public void SetInactive(){
		gameObject.SetActive (false);
	}
}
