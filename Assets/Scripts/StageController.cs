﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class StageController : MonoBehaviour {

	private const int BTN_WAIT = 0;
	private const int BTN_READY = 1;
	private const int BTN_GO = 2;
	private const int BTN_ERROR = 3;

    [System.Serializable]
    public class PlayerSpawnEvent : UnityEvent{}

    [System.Serializable]
    public class VictoryEvent : UnityEvent{}

    [System.Serializable]
    public class TransmissionEvent : UnityEvent{}

    [Header("Player Indices")]
    public int player1Index = 0;
    public int player2Index = 1;

    [Header("Player Heights")]
    public float player1Height = -250;
    public float player2Height = -300;

    [Header("UI Indicators")]
    public GameObject awayIndicator1;
    public GameObject awayIndicator2;

    public GameObject frontIndicator1;
    public GameObject frontIndicator2;

    public GameObject speedIndicator1;
    public GameObject speedIndicator2;
	private GameObject[,] sequences = { { null, null, null, null}, {null, null, null, null} };
	//private GameObject[] sequence2 = { null, null, null, null};

    public GameObject transmissionIndicator; 

    [Header("Transmission")]
    public float transmissionThreshold = 100;
    public float transmissionSpeed = 10;

    [Header("Scroll Settings")]
    public float scrollSmoothness = 0.05f;
    public float playerDistanceFromCenter = 200;

    public float transmissionsToWin = 3;
    public float distanceSinceLastTransmissionToWin = 2000;
    public float transmissionDistance = 10000; /**< Distance between transmissions*/

    public GameObject playerPrefab;
    public GameObject goalPrefab;

    public PlayerController currentPlayer;
    public PlayerController nextPlayer;
    private PlayerController orphanPlayer; /**< current player once the transmission is completed, until it leaves the screen.*/

    public float scrollSpeed = 0;
    public Transform[] backgroundTiles;

    public PlayerSpawnEvent OnPlayerSpawn;
    public TransmissionEvent OnTransmission;
    public VictoryEvent OnVictory;

    private GameObject goal = null;
    private bool firstPlayer = false;
    private int firstTile = 0;
    private float distanceCovered = 0;
    private float distanceCoveredSinceLastTransmission = 0;

    private float timerfail1 = 0;
    private float timerfail2 = 0;
	private float timergood1 = 0;
    private float timergood2 = 0;
    private uint previousBtnIndex1 = 0;
    private uint previousBtnIndex2 = 0;
    private string failbtn1, failbtn2;


    public void Awake() {
    	// setup sequence buttons
    	if (speedIndicator1 != null) {
    		for (int i=0; i<4; i++) {
    			sequences[0,i] = speedIndicator1.transform.parent.GetChild(1 + i).gameObject;
    		}
    	}
		if (speedIndicator2 != null) {
    		for (int i=0; i<4; i++) {
    			sequences[1,i] = speedIndicator2.transform.parent.GetChild(1 + i).gameObject;
    		}
    	}
		HideButtonsSequenceDisplay();
    	SetupSequenceDisplay(currentPlayer);
    }

    public void Update(){
    
        // move players:
        if (currentPlayer != null) currentPlayer.transform.localPosition += Vector3.right * currentPlayer.speed;
        if (nextPlayer != null) nextPlayer.transform.localPosition += Vector3.right * nextPlayer.speed;
        if (orphanPlayer != null) orphanPlayer.transform.localPosition += Vector3.right * orphanPlayer.speed;

        // calculate average player position:
        /*float avgPosition = 0;
        int playerCount = 0;

        if (currentPlayer != null){
            avgPosition += currentPlayer.transform.localPosition.x;
            playerCount++;
        }

        if (nextPlayer != null){
            avgPosition += nextPlayer.transform.localPosition.x;
            playerCount++;
        }

        avgPosition /= playerCount;*/

        float avgPosition = (currentPlayer != null) ? currentPlayer.transform.localPosition.x : 0;

        // move scroll:
        Scroll(Mathf.Max(0,(avgPosition + playerDistanceFromCenter) * scrollSmoothness));

        DestroyPlayerIfNeeded();
        SpawnPlayerIfNeeded();

        AttemptTransmission();

        UpdateAwayIndicators();
        UpdateFrontIndicators();
        // not finished!! UpdateButtonIndicators();
        CheckConditionsToWin();
    }

    public void Scroll (float amount) {

        // Update scroll:
        for (int i = 0; i < backgroundTiles.Length; ++i)
            backgroundTiles[i].localPosition -= Vector3.right * amount; 

        // Move tiles around to simulate infinite scroll:
        float offset = backgroundTiles[firstTile].localPosition.x - (-640-320);
        if (offset < 0){
            backgroundTiles[firstTile].localPosition = new Vector3(640+offset,0,0);
            firstTile = (firstTile+1) % backgroundTiles.Length;
        }

        // move players with scroll:
        if (currentPlayer != null) currentPlayer.transform.localPosition -= Vector3.right*amount;
        if (nextPlayer != null) nextPlayer.transform.localPosition -= Vector3.right*amount;
        if (orphanPlayer != null) orphanPlayer.transform.localPosition -= Vector3.right*amount;

        // move goal:
        if (goal != null) goal.transform.localPosition -= Vector3.right*amount;

        // increase distance covered
        distanceCovered += amount;

        if (nextPlayer == null)
            distanceCoveredSinceLastTransmission += amount;

	}

    public void CheckConditionsToWin(){

        if (distanceCoveredSinceLastTransmission > distanceSinceLastTransmissionToWin && transmissionsToWin == 0){
            transmissionsToWin = -1;
            SpawnGoal();
        }
        
        if (transmissionsToWin == -1 && goal != null){

            if (currentPlayer != null && goal.transform.localPosition.x < currentPlayer.transform.localPosition.x)
                OnVictory.Invoke();

        }
    }

    public void SpawnPlayerIfNeeded(){

        if (distanceCoveredSinceLastTransmission > transmissionDistance){
            distanceCoveredSinceLastTransmission -= transmissionDistance;
            SpawnPlayer();
        }

    }

    public void DestroyPlayerIfNeeded(){

        if (orphanPlayer != null && orphanPlayer.transform.localPosition.x < -800){
            GameObject.Destroy(orphanPlayer);
            orphanPlayer = null;
        }
    }

    public void SpawnPlayer(){

        GameObject player = GameObject.Instantiate(playerPrefab,gameObject.transform,false);

        player.transform.localPosition = new Vector3(1200,firstPlayer ? player1Height:player2Height,0);
        
        player.GetComponent<InputController>().SetRandomSequence();
        player.GetComponent<Renderer>().sortingOrder = firstPlayer ? 0:1;
        player.GetComponent<InputController>().OnInputFail.AddListener(ButtonFailUpdateDisplay);
		player.GetComponent<InputController>().OnInputOk.AddListener(ButtonOkUpdateDisplay);
        
        nextPlayer = player.GetComponent<PlayerController>();
        nextPlayer.speed = currentPlayer.speed;
        nextPlayer.input.playerIndex = firstPlayer ? player1Index : player2Index;
		// refresh interface for sequence
		SetupSequenceDisplay(nextPlayer);

        if (firstPlayer){
            nextPlayer.OnSpeedChange.AddListener(SetSpeedIndicator1);
        }else{
            nextPlayer.OnSpeedChange.AddListener(SetSpeedIndicator2);
        }

       
        // next player spawned
        firstPlayer = !firstPlayer; 
        
        OnPlayerSpawn.Invoke();
    }

    public void SetSpeedIndicator1(float speed){
        speedIndicator1.GetComponent<Slider>().value = speed;
    }
public void SetSpeedIndicator2(float speed){
        speedIndicator2.GetComponent<Slider>().value = speed;
    }

    public void SpawnGoal(){
         goal = GameObject.Instantiate(goalPrefab,gameObject.transform,false);
         goal.transform.localPosition = new Vector3(1200,-185);
    }

    public void AttemptTransmission(){

        if (currentPlayer != null && nextPlayer != null){

            Image indicator = transmissionIndicator.GetComponent<Image>();

            if (Mathf.Abs(currentPlayer.transform.localPosition.x - nextPlayer.transform.localPosition.x) < transmissionThreshold){

                transmissionIndicator.SetActive(true);
                indicator.fillAmount += transmissionSpeed * Time.deltaTime;

                if (indicator.fillAmount >= 1){

                    indicator.fillAmount = 0;

                    currentPlayer.GetComponent<InputController>().enabled = false;
                    orphanPlayer = currentPlayer;
                    currentPlayer = nextPlayer;
                    nextPlayer = null;

                    transmissionsToWin--;
        
                    OnTransmission.Invoke();
                }
            } else {
                transmissionIndicator.SetActive(false);
                indicator.fillAmount = 0;
            }
        }else
            transmissionIndicator.SetActive(false);
    }

	public void UpdateButtonIndicators() {
		int playeridx = 0;
		int deltax, deltay;
		if (currentPlayer != null) {
			if ((currentPlayer.input.playerIndex == 1) || (currentPlayer.input.playerIndex == 3)) {
				playeridx = 1;
			}
			sequences[playeridx, currentPlayer.input.nextButtonIndex].transform.GetChild(BTN_READY).gameObject.SetActive(true);
			getButtonCoordinates(out deltax, out deltay, currentPlayer.input.nextButtonName);
			sequences[playeridx, currentPlayer.input.nextButtonIndex].transform.GetChild(BTN_READY).localPosition =
				new Vector3(deltax, deltay, 1);
			if (timerfail1 > 0) {
				timerfail1 -= (0.1f * Time.deltaTime);
				sequences[playeridx, currentPlayer.input.nextButtonIndex].transform.GetChild(BTN_ERROR).gameObject.SetActive(true);
				getButtonCoordinates(out deltax, out deltay, failbtn1);
				sequences[playeridx, currentPlayer.input.nextButtonIndex].transform.GetChild(BTN_ERROR).localPosition =
					new Vector3(deltax, deltay, 2);
			} else {
				sequences[playeridx, currentPlayer.input.nextButtonIndex].transform.GetChild(BTN_ERROR).gameObject.SetActive(false);
			getButtonCoordinates(out deltax, out deltay, currentPlayer.input.nextButtonName);
			sequences[playeridx, currentPlayer.input.nextButtonIndex].transform.GetChild(BTN_READY).localPosition =
				new Vector3(deltax, deltay, 1);
			}
		}
		playeridx = 0;
		if (nextPlayer != null) {
			if ((nextPlayer.input.playerIndex == 1) || (nextPlayer.input.playerIndex == 3)) {
				playeridx = 1;
			}
			sequences[playeridx, nextPlayer.input.nextButtonIndex].transform.GetChild(BTN_READY).gameObject.SetActive(true);
			getButtonCoordinates(out deltax, out deltay, nextPlayer.input.nextButtonName);
			sequences[playeridx, nextPlayer.input.nextButtonIndex].transform.GetChild(BTN_READY).localPosition =
				new Vector3(deltax, deltay, 1);
		}
	}
    
    public void UpdateAwayIndicators(){

        if (firstPlayer){
            if (currentPlayer != null && currentPlayer.transform.localPosition.x < -700){
                awayIndicator1.SetActive(true);
            }else{
                awayIndicator1.SetActive(false);
            }

            if (nextPlayer != null && nextPlayer.transform.localPosition.x < -700){
                awayIndicator2.SetActive(true);
            }else{
                awayIndicator2.SetActive(false);
            }
        }


        if (!firstPlayer){
            if (currentPlayer != null && currentPlayer.transform.localPosition.x < -700){
                awayIndicator2.SetActive(true);
            }else{
                awayIndicator2.SetActive(false);
            }

            if (nextPlayer != null && nextPlayer.transform.localPosition.x < -700){
                awayIndicator1.SetActive(true);
            }else{
                awayIndicator1.SetActive(false);
            }
        }

    }

	public void ButtonOkUpdateDisplay(string btn, int playeridx) {
		if ((playeridx == 1) || (playeridx == 3)) {
			timergood1 = GameDictionary.FAIL_TIME_MAX;
			previousBtnIndex1 = currentPlayer.input.nextButtonIndex;
			Debug.Log("prev:" + previousBtnIndex1.ToString());
		} else {
			timergood2 = GameDictionary.FAIL_TIME_MAX;
			if (nextPlayer != null) {
				previousBtnIndex1 = nextPlayer.input.nextButtonIndex;
			}
		}

    }

	public void ButtonFailUpdateDisplay(string btn, int playeridx) {
		if ((playeridx == 1) || (playeridx == 3)) {
			timerfail1 = GameDictionary.FAIL_TIME_MAX;
		} else {
			timerfail2 = GameDictionary.FAIL_TIME_MAX;
		}
	}

    public void UpdateFrontIndicators(){

        if (firstPlayer){
            if (currentPlayer != null && currentPlayer.transform.localPosition.x > 700){
                frontIndicator1.SetActive(true);
            }else{
                frontIndicator1.SetActive(false);
            }

            if (nextPlayer != null && nextPlayer.transform.localPosition.x > 700){
                frontIndicator2.SetActive(true);
            }else{
                frontIndicator2.SetActive(false);
            }
        }


        if (!firstPlayer){
            if (currentPlayer != null && currentPlayer.transform.localPosition.x > 700){
                frontIndicator2.SetActive(true);
            }else{
                frontIndicator2.SetActive(false);
            }

            if (nextPlayer != null && nextPlayer.transform.localPosition.x > 700){
                frontIndicator1.SetActive(true);
            }else{
                frontIndicator1.SetActive(false);
            }
        }

    }

    private void HideButtonsSequenceDisplay() {
    	for (int j=0; j<2; j++) {
			for (int i=0; i<4; i++) {
					sequences[j,i].transform.GetChild(BTN_WAIT).gameObject.SetActive(false);
					sequences[j,i].transform.GetChild(BTN_READY).gameObject.SetActive(false);
					sequences[j,i].transform.GetChild(BTN_GO).gameObject.SetActive(false);
					sequences[j,i].transform.GetChild(BTN_ERROR).gameObject.SetActive(false);
				
			}
		}
    }

	private void SetupSequenceDisplay(PlayerController pc) {
		// Hide all buttons
		int deltax = 0;
		int deltay = 0;
		int playeridx = 0;

		if ((pc.input.playerIndex == 1) || (pc.input.playerIndex == 3)) {
			playeridx = 1;
		}
		for (int i=0; i<4; i++) {
			sequences[playeridx,i].transform.GetChild(BTN_READY).gameObject.SetActive(false);
			sequences[playeridx,i].transform.GetChild(BTN_GO).gameObject.SetActive(false);
			sequences[playeridx,i].transform.GetChild(BTN_ERROR).gameObject.SetActive(false);

			sequences[playeridx,i].transform.GetChild(BTN_WAIT).gameObject.SetActive(true);

			getButtonCoordinates(out deltax, out deltay, pc.input.sequence.buttons[i]);

			sequences[playeridx,i].transform.GetChild(BTN_WAIT).localPosition = 
				new Vector3 (deltax, deltay, 0);
		}

	}

	private void getButtonCoordinates(out int x, out int y, string btn) {
		
		switch(btn) {
			case "Up":
				x = 0;
				y = 24;
				break;
			case "Down":
				x = 0;
				y = -24;
				break;
			case "Left":
				x = -24;
				y = 0;
				break;
			default: // right
				x = 24;
				y = 0;
				break;				
			}
	}
}
