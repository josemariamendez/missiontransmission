﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class UIScaleEffect : MonoBehaviour {

    public float amplitude = 0.2f;
    public float speed = 10;

    RectTransform rt;   

	// Use this for initialization
	void Awake () {
        rt = GetComponent<RectTransform>();
	}
	
	// Update is called once per frame
	void Update () {
        rt.localScale = Vector3.one * (1+Mathf.Sin(Time.realtimeSinceStartup*speed)*amplitude);
	}
}
