﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "Sequence", menuName = "MissionTransmission/Sequence", order = 0)]
public class InputSequence : ScriptableObject
{
    public RuntimeAnimatorController animationController; 
    public string[] buttons;
}

