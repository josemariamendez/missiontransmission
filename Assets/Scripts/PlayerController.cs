﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof (Animator))]
public class PlayerController : MonoBehaviour {

    [System.Serializable]
    public class SpeedChangeEvent : UnityEvent<float>{}

    public InputController input;

    public AnimationCurve deltaToSpeed;
    public AnimationCurve timeToPenalty;
    
    public float speed = 10;
    public float stamina = 100;

    public SpeedChangeEvent OnSpeedChange;

    private Animator animator;

    void Awake(){
        animator = GetComponent<Animator>();
    }

	// Update is called once per frame
	void Update () {
        if (input != null){

            // map delta to change in speed:
            speed += deltaToSpeed.Evaluate(input.Delta) * Time.deltaTime;
        
            // penalize large periods without pressing a button:
            speed -= timeToPenalty.Evaluate(input.CurrentInterval) * Time.deltaTime;
       
            // keep speed between min/max values
            speed = Mathf.Clamp(speed,0,40);

            animator.speed = speed*0.1f;

            OnSpeedChange.Invoke(speed);
        }
	}

    public void OnFail(){
        speed -= 1;
    }
}
