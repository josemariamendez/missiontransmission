﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class UIColorBlink : MonoBehaviour {

    public float blinkDuration = 0.5f;
    public Color blinkColor = Color.red;
	
    private Color initialColor;
    private Image image;
    private float timeSinceLastBlink = 0;

	void Awake () {
        image = GetComponent<Image>();
		initialColor = image.color;
	}

    public void Update(){
        timeSinceLastBlink += Time.deltaTime;
        if (timeSinceLastBlink >= blinkDuration)
            image.color = initialColor; 
    }

    public void Blink(){
        image.color = blinkColor;
        timeSinceLastBlink = 0;
    }
}
