﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLogic : MonoBehaviour {

	public GameObject IntroSection = null;
	public GameObject MenuSection = null;
	public GameObject PlayerConfigSection = null;
	public GameObject TutorialSection = null;
	public GameObject GameSection = null;
	public GameObject EndGameSection = null;
	public GameObject PauseSection = null;
	public GameObject CreditsSection = null;

	private bool gameIsRunning;
	private int gameState;
	private int optionSelected;
	private GameObject previousSection = null;
	private GameObject actualSection = null;
	private GameObject nextSection = null;

	// Use this for initialization
	void Start () {
		Application.targetFrameRate = 60; // we must aim for 60 frames, less will be sluggy	

		gameIsRunning = true;
		gameState = GameDictionary.STATE_INIT;
		optionSelected = GameDictionary.OPTION_NONE;

		StartCoroutine(GameFSM());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void Quit() {
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_WEBPLAYER
		Application.OpenURL(GameDictionary.webplayerQuitURL);
#else
		Application.Quit();
#endif
	}

	private void UpdateSections() {
		IntroSection.SetActive(false);
		MenuSection.SetActive(false);
		PlayerConfigSection.SetActive(false);
		TutorialSection.SetActive(false);
		GameSection.SetActive(false);
		EndGameSection.SetActive(false);
		PauseSection.SetActive(false);
		CreditsSection.SetActive(false);

		previousSection = actualSection;
		actualSection = nextSection;
		nextSection = null;
		actualSection.SetActive(true);

		optionSelected = GameDictionary.OPTION_NONE;
	}

	public IEnumerator GameFSM() {
		while (gameIsRunning) {
			switch(gameState) {
			case GameDictionary.STATE_INIT:
				previousSection = null;
				actualSection = null;
				nextSection = IntroSection;
				gameState = GameDictionary.STATE_INTRO_IN;
				break;
			case GameDictionary.STATE_INTRO_IN:
				UpdateSections();
				gameState = GameDictionary.STATE_INTRO;
				break;
			case GameDictionary.STATE_INTRO:
				if (optionSelected != GameDictionary.OPTION_NONE) {
					gameState = GameDictionary.STATE_INTRO_OUT;
				}
				break;
			case GameDictionary.STATE_INTRO_OUT:
				nextSection = MenuSection;
				gameState = GameDictionary.STATE_MENU_IN;
				break;
			case GameDictionary.STATE_MENU_IN:
				UpdateSections();
				gameState = GameDictionary.STATE_MENU;
				break;
			case GameDictionary.STATE_MENU:
				if (optionSelected != GameDictionary.OPTION_NONE) {
					gameState = GameDictionary.STATE_MENU_OUT;
				}
				break;
			case GameDictionary.STATE_MENU_OUT:
				switch(optionSelected) {
				case GameDictionary.OPTION_START:
					nextSection = TutorialSection;
					gameState = GameDictionary.STATE_TUTORIAL_IN;
					break;
				case GameDictionary.OPTION_CREDITS:
					nextSection = CreditsSection;
					gameState = GameDictionary.STATE_CREDITS_IN;
					break;
				case GameDictionary.OPTION_EXIT:
					Quit();
					break;
				}
				break;
			case GameDictionary.STATE_PLAYER_CONFIG_IN:
				UpdateSections();
				gameState = GameDictionary.STATE_PLAYER_CONFIG;
				break;
			case GameDictionary.STATE_PLAYER_CONFIG:
				if (optionSelected != GameDictionary.OPTION_NONE) {
					gameState = GameDictionary.STATE_PLAYER_CONFIG_OUT;
				}
				break;
			case GameDictionary.STATE_PLAYER_CONFIG_OUT:
				switch(optionSelected) {
				case GameDictionary.OPTION_BACK:
					nextSection = MenuSection;
					gameState = GameDictionary.STATE_MENU_IN;
					break;
				case GameDictionary.OPTION_CONTINUE:
					nextSection = TutorialSection;
					gameState = GameDictionary.STATE_TUTORIAL_IN;
					break;
				}
				break;
			case GameDictionary.STATE_TUTORIAL_IN:
				UpdateSections();
				gameState = GameDictionary.STATE_TUTORIAL;
				break;
			case GameDictionary.STATE_TUTORIAL:
				if (optionSelected != GameDictionary.OPTION_NONE) {
					gameState = GameDictionary.STATE_TUTORIAL_OUT;
				}
				break;
			case GameDictionary.STATE_TUTORIAL_OUT:
				switch(optionSelected) {
				case GameDictionary.OPTION_BACK:
					nextSection = MenuSection;
					gameState = GameDictionary.STATE_MENU_IN;
					break;
				case GameDictionary.OPTION_CONTINUE:
					nextSection = GameSection;
					gameState = GameDictionary.STATE_GAME_IN;
					break;
				}
				break;
			case GameDictionary.STATE_GAME_IN:
				UpdateSections();
				gameState = GameDictionary.STATE_GAME;
				break;
			case GameDictionary.STATE_GAME:
				if (optionSelected != GameDictionary.OPTION_NONE) {
					gameState = GameDictionary.STATE_GAME_OUT;
				}
				break;
			case GameDictionary.STATE_GAME_OUT:
				switch(optionSelected) {
				case GameDictionary.OPTION_PAUSE:
					nextSection = PauseSection;
					gameState = GameDictionary.STATE_PAUSE_IN;
					break;
        case GameDictionary.OPTION_EXIT:{
            nextSection = MenuSection;
            gameState = GameDictionary.STATE_MENU_IN;
        }break;
				case GameDictionary.OPTION_ENDGAME:
					nextSection = EndGameSection;
					gameState = GameDictionary.STATE_END_GAME_IN;
					break;
				}
				break;
			case GameDictionary.STATE_END_GAME_IN:
				UpdateSections();
				gameState = GameDictionary.STATE_END_GAME;
				break;
			case GameDictionary.STATE_END_GAME:
				if (optionSelected != GameDictionary.OPTION_NONE) {
					gameState = GameDictionary.STATE_END_GAME_OUT;
				}
				break;
			case GameDictionary.STATE_END_GAME_OUT:
				nextSection = MenuSection;
				gameState = GameDictionary.STATE_MENU_IN;
				break;
			case GameDictionary.STATE_PAUSE_IN:
				UpdateSections();
				gameState = GameDictionary.STATE_PAUSE;
				break;
			case GameDictionary.STATE_PAUSE:
				if (optionSelected != GameDictionary.OPTION_NONE) {
					gameState = GameDictionary.STATE_PAUSE_OUT;
				}
				break;
			case GameDictionary.STATE_PAUSE_OUT:
				switch(optionSelected) {
				case GameDictionary.OPTION_CONTINUE:
					nextSection = GameSection;
					gameState = GameDictionary.STATE_GAME_IN;
					break;
				case GameDictionary.OPTION_EXIT:
					nextSection = MenuSection;
					gameState = GameDictionary.STATE_MENU_IN;
					break;
				}
				break;
			case GameDictionary.STATE_CREDITS_IN:
				UpdateSections();
				gameState = GameDictionary.STATE_CREDITS;
				break;
			case GameDictionary.STATE_CREDITS:
				if (optionSelected != GameDictionary.OPTION_NONE) {
					gameState = GameDictionary.STATE_CREDITS_OUT;
				}
				break;
			case GameDictionary.STATE_CREDITS_OUT:
				nextSection = MenuSection;
				gameState = GameDictionary.STATE_MENU_IN;
				break;
			}
			yield return new WaitForSeconds(GameDictionary.GAMEFSM_UPDATE_RATE);
		}
		yield break;
	}

	public void btnContinueClick() {
		optionSelected = GameDictionary.OPTION_CONTINUE;
	}

	public void btnStartClick() {
		optionSelected = GameDictionary.OPTION_START;
	}

	public void btnCreditsClick() {
		optionSelected = GameDictionary.OPTION_CREDITS;
	}

	public void btnPauseClick() {
		optionSelected = GameDictionary.OPTION_PAUSE;
	}

	public void btnExitClick() {
		optionSelected = GameDictionary.OPTION_EXIT;
	}

	public void btnBackClick() {
		optionSelected = GameDictionary.OPTION_BACK;
	}

	public void actionEndGame() {
		optionSelected = GameDictionary.OPTION_ENDGAME;
	}
}
